# Servidor de URLs de videos

Servidor de datos sobre videos, asociados a proyectos, implementado usando el framework [FastAPI](https://fastapi.tiangolo.com/).


# Tabla de contenidos

- [Configurar y correr el servidor](#configurar-y-correr-el-servidor)
- [Documentación interactiva de la API generada por FastAPI](#documentación-interactiva-de-la-api-generada-por-fastapi)
- [Migraciones](#migraciones)
- [Acceso a la API](#acceso-a-la-api)
- [CORS (Cross-origin resource sharing)](#cors-cross-origin-resource-sharing)
- [Estructuras de los JSON](#estructuras-de-los-json)
- [Endpoints](#endpoints)
- [Detalles de implementación y ejemplos](#detalles-de-implementación-y-ejemplos)
  * [HelpVideo](#helpvideo-1)
    + [Obtener un HelpVideo especifico](#obtener-un-helpvideo-especifico)
    + [Obtener la lista de HelpVideo's asociados a un Project](#obtener-la-lista-de-helpvideos-asociados-a-un-project)
    + [Crear un HelpVideo nuevo](#crear-un-helpvideo-nuevo)
    + [Modificar un HelpVideo](#modificar--algunos--campos-de-un-helpvideo)
  * [Project](#project-1)
    + [Crear un Project nuevo](#crear-un-project-nuevo)
    + [Modificar un Project](#modificar--algunos--campos-de-un-project)
    + [Eliminar un Project](#eliminar-un-project)





# Instalación del Framework y librerias
Correr el comando:

```bash
pip install -r requirements.txt
```

> **Nota:** Se aconseja utilizar un `virtualenv`

# Configurar y correr el servidor

## Configuración e inicialización de credenciales
La App utiliza variables de entorno para las credenciales y datos sobre la configuración de la base de datos.

Las mismas se encuentran en el archivo `init_server.sh` en el directorio raiz del proyecto.


Veamos el contenido del mismo:

```bash
#!/bin/bash

# API CREDENTIALS
export AIT_CENTRAL_USERNAME='stanleyjobson'
export AIT_CENTRAL_PASSWORD='swordfish'

# DATABASE CREDENTIALS
export AIT_CENTRAL_DB_USERNAME='root'
export AIT_CENTRAL_DB_PASSWORD='123456'
export AIT_CENTRAL_DB_HOST='localhost'
export AIT_CENTRAL_DB_PORT='3306'
export AIT_CENTRAL_DB_NAME='boxer_common'
```

* `AIT_CENTRAL_USERNAME` y `AIT_CENTRAL_PASSWORD`: son las credenciales para acceder a la API, los requests realizados deben contener esta información, tambien se piden estas credenciales si se quiere acceder a la documentación interactiva mediante el navegador.

* Las variables `AIT_CENTRAL_DB_*` se refieren a los datos de la configuración de la base de datos que el usuario tiene corriendo en su sistema.

### Inicialización de variables de entorno para la configuración
Una vez modificado el archivo `init_server.sh` se debera correr el siguiente comando:

```bash
. init_server.sh
```

> **Nota**: Es importante inicializar con `. init_server.sh`  en vez de `./init_server.sh`, el primero setea las variables de entorno dentro de la instancia de Bash que estamos corriendo en el momento. El segundo crea una nueva instancia de Bash, setea las variables de entorno en esa instancia y cuando termina de correr el script se termina la instancia de Bash y se pierden las variables que queriamos setear.


## Correr el servidor

```bash
./run_server.sh
```


# Documentación interactiva de la API generada por FastAPI
Asumiendo que se esta corriendo el servidor en: `127.0.0.1:8000` (viene configurado por defecto asi en `./run_server.sh` pero puede cambiarse), para acceder a la documentación interactiva que genera FastAPI se pueden visitar las URLS:

* `http://127.0.0.1:8000/docs` (provided by Swagger UI)

* `http://127.0.0.1:8000/redoc` (provided by ReDoc)

La documentación interactiva provee de funcionalidades que permiten usar **por completo** la API desde un navegador.



# Migraciones

El proceso se divide en 2, *crear las migraciones*, y *aplicar las migraciones*.

## Para crearlas correr el script:

```bash
./makemigrations.sh -m MESSAGE
```

> Nota: MESSAGE es requerido, se utiliza para el filename de la migración, debe contener unas pocas palabras sin espacios.

#### Ejemplo

```bash
./makemigrations.sh -m helpvideoidchange
```


## Para aplicar migraciones:

```bash
./migrate.sh
```

## En caso del error "FAILED: Can't locate revision identified by 'migrationname' error":

Este error indica que no encuentra el ultimo archivo de migracion dentro del directorio "alembic/versions" (quizas fue eliminado).

* Una solución es recuperar ese archivo faltante y meterlo en el directorio "alembic/versions" en el directorio raiz del proyecto.

* Si no es posible, otra solución es hacer un DROP a la tabla 'alembic\_version' que crea Alembic dentro de la base de datos y borrar los archivos dentro del directorio "alembic/versions", para esto se puede correr el script:
```bash
./clean_migrations.sh
```


# Acceso a la API

Para acceder a la API se debe agregar en el header de cada request un campo `Authorization`
que incluya las credenciales, respetando el siguiente formato: 

```
'Authorization': 'Basic ' + btoa(\`${username}:${password}\`)
```


### Ejemplo usando Javascript puro (adaptar a Axios o libreria a elección)

```javascript

let url = 'http://127.0.0.1:8000/videos/'
let username = 'stanleyjobson'
let password = 'swordfish'

fetch(url, {
	headers: {
    	'Authorization': 'Basic ' + btoa(`${username}:${password}`)
    }
})
.then(response => response.json())
.then(json => console.log(json));

```


# CORS (Cross-origin resource sharing)
La app viene configurada por defecto para que se pueda acceder desde cualquier IP ("*"),
esta configuración debe ser cambiada en producción, la misma puede ser editada dentro del archivo main.py, usando la variable ya existente `origins`.


```python
origins = [
    # Contains the allowed ip's
    "http://localhost",
    "http://localhost:8080",
    "*"
]
```


# Estructuras de los JSON

### HelpVideo
```json
{
    "video_id": "int",                "id del video dentro de la Base de Datos. (PK)"
    "video_title": "string",          "Titulo del video (e.g: 'Compra rapida')"
    "video_description": "string",    "Breve descripción de lo que se muestra en el video"
    "video_url": "string",            "URL donde esta alojado el video"
    "project_id": "int"               "id del proyecto al que pertenece el video"
}
```

### Project
```json
{
    "project_id": "int",              "id del proyecto dentro de la Base de Datos (PK)"
    "name": "string"                  "Nombre del proyecto (e.g: 'Boxer', 'Duck') (UNIQUE)"
}
```

# Endpoints

### HelpVideos

| Método     | URI                                 | Descripción                                        |
|------------|-------------------------------------|----------------------------------------------------|
| **GET**    | `/videos/`                            | Obtiene una lista de todos los HelpVideos.         |
| **GET**    | `/videos/{video_id:int}`           | Obtiene el HelpVideo correspondiente a `video_id`. |
| **GET**    | `/videos/project/{project_id:int}` | Devuelve los HelpVideos que pertenecen al proyecto con id `project_id`. |
| **POST**   | `/videos/`                            | Crea un HelpVideo nuevo.                           |
| **PATCH**  | `/videos/update/{video_id:int}`    | Modifica el HelpVideo con id `video_id`, (enviar *solamente* los campos a modificar). |
| **DELETE** | `/videos/delete/{video_id:int}`    | Elimina el HelpVideo con id `video_id`.           |

### Projects

| Método     |URI                                   | Descripción                              |
|------------|--------------------------------------|------------------------------------------|
| **GET**    | `/projects/`                           | Obtiene una lista de todos los Projects. |
| **GET**    | `/projects/{project_id:int}`        | Obtiene el Project correspondiente a `project_id`. |
| **POST**   | `/projects/`                           | Crea un Project nuevo.                   |
| **PATCH**  | `/projects/update/{project_id:int}` | Modifica el Project con id `project_id`, (enviar *solamente* los campos a modificar). |
| **DELETE** | `/projects/delete/{project_id:int}` | Elimina el Project con id `project_id`. |


# Detalles de implementación y ejemplos


## HelpVideo

### Obtener un HelpVideo especifico
- `GET /videos/{video_id:int}`

#### *Ejemplo:*

* REQUEST: `GET /videos/1`

* RESPONSE: `200 OK`

##### *JSON* Response:

```json
{
  "video_id": 1,
  "video_title": "Informes",
  "video_description": "Muestra como se maneja informes",
  "video_url": "https://vimeo.com/?v=12345678910",
  "project_id": 1
}
```

<br>

### Obtener la lista de HelpVideo's asociados a un Project
- `GET /videos/project/{project_id:int}`

#### *Ejemplo:*

* REQUEST: `GET /videos/project/1`

* RESPONSE: `200 OK`

##### *JSON* Response:

```json
[
  {
    "project_id": 1,
    "video_title": "Informes",
    "video_description": "Muestra como se maneja informes",
    "video_url": "https://vimeo.com/Je312KjrW3",
    "video_id": 1
  },
  {
    "project_id": 1,
    "video_title": "Ventas",
    "video_description": "Muestra como se maneja ventas",
    "video_url": "https://vimeo.com/?v=4444444",
    "video_id": 2
  },
  {
    "project_id": 1,
    "video_title": "Presupuesto",
    "video_description": "pasos para presupuestar",
    "video_url": "https://youtu.be/M3ijDK",
    "video_id": 9
  }
]
```

<br>

### Crear un HelpVideo nuevo
- `POST /videos/`

Cuando se crea un video nuevo, el `video_id` se genera por si solo en la base de datos
por lo tanto no se debe asignar dentro del JSON.

Luego de realizar el POST, si todo salio bien, el backend devuelve "201 Created" y el JSON del video creado, ahora si, con su `video_id` asignado por la BD.


#### *Ejemplo:*

* REQUEST: `POST /videos/`
##### _JSON_ Request:

```json
{
  "video_title": "Informes",
  "video_description": "Muestra como se maneja informes",
  "video_url": "https://vimeo.com/?v=12345678910",
  "project_id": 1
}
```

* RESPONSE: `201 Created`

##### _JSON_ Response:
```json
{
  "video_id": 24,
  "video_title": "Informes",
  "video_description": "Muestra como se maneja informes",
  "video_url": "https://vimeo.com/?v=12345678910",
  "project_id": 1
}
```

<br>


### Modificar _algunos_ campos de un HelpVideo

- `PATCH /videos/update/{video_id:int}`

A diferencia de **PUT**, en **PATCH** solo se deben agregar dentro del JSON el/los campos a modificar.


#### *Ejemplo 1:*
Se quiere modificar la url del video con `video_id:1`

* REQUEST: `PATCH /videos/update/1`

##### _JSON_ Request:
```json
{
    "video_url": "https://vimeo.com/Je312KjrW3"
}
```

* RESPONSE: `200 OK`

##### _JSON_ Response:
```json
{
  "project_id": 1,
  "video_title": "Informes",
  "video_description": "Muestra como se maneja informes",
  "video_url": "https://vimeo.com/Je312KjrW3",
  "video_id": 1
}
```


#### *Ejemplo 2:*
Se quiere cambiar la descripción y el titulo del HelpVideo con `video_id:2`

* REQUEST: `PATCH /videos/update/2`

##### _JSON_ Request:
```json
{
    "video_title": "Presupuesto",
    "video_description": "Explica como presupuestar"
}
```

* RESPONSE: `200 OK`

##### _JSON_ Response:
```json
{
  "video_id": 2,
  "video_title": "Presupuesto",
  "video_description": "Explica como presupuestar",
  "video_url": "https://vimeo.com/Je312KjrW3",
  "project_id": 1
}
```

<br>


## Project

### Crear un Project nuevo
- `POST /projects/`

Cuando se crea un project nuevo **tener en cuenta que el campo `name` es de tipo "unico"** (no puede repetirse dentro de la BD), ademas, al igual que con los HelpVideo's, el id `project_id` se genera por si solo en la base de datos
por lo tanto no se debe asignar dentro del JSON.

Luego de realizar el POST, si todo salio bien, el backend devuelve "201 Created" y el JSON del project creado, ahora si, con su `project_id` asignado por la BD.


#### *Ejemplo 1 (Exito):*

* REQUEST: `POST /project/`
##### _JSON_ Request:

```json
{
  "name": "Duck"
}
```

* RESPONSE: `201 Created`

##### _JSON_ Response:
```json
{
  "name": "Duck",
  "project_id": 2
}
```


#### *Ejemplo 2 (Fallo):*
En este escenario ya existe un Project con `name:"Duck"`

* REQUEST: `POST /project/`
##### _JSON_ Request:

```json
{
  "name": "Duck"
}
```

* RESPONSE: `409 Conflict`

##### _JSON_ Response:
```json
{
  "detail": "Project name already taken."
}
```

<br>


### Modificar *algunos* campos de un Project
- `PATCH /projects/update/{project_id:int}`

Para modificar un Project se deben tener ciertos cuidados:

1) El campo `name` es unico, luego no debe contener un nombre tomado por otro proyecto, si se intenta usar el nombre de un proyecto que ya existe la API devolverá un error.

2) Si el Project tiene videos asociados no se podrá modificar el `project_id`, ya que se perderia la asociación.


#### *Ejemplo 1 (Exito):*
Queremos modificar el nombre `name` del proyecto con `project_id:1`:

* REQUEST: `PATCH /projects/update/1`

##### _JSON_ Request:
```json
{
    "name": "Boxer"
}
```

* RESPONSE: `200 OK`

##### _JSON_ Response:
```json
{
    "project_id": 1,
    "name": "Boxer"
}
```

<br>

#### *Ejemplo 2 (Fallo):*
Queremos modificar el `project_id` de un Project con `project_id:2` pero tiene videos asociados:

* REQUEST: `PATCH /projects/update/2`

* RESPONSE: `409 Conflict`

##### _JSON_ Response:
```json
{
    "detail": "Cannot delete or update a parent row: a foreign key constraint fails"
}
```

<br>

### Eliminar un Project
- `DELETE /projects/delete/{project_id:int}`

Para eliminar un Project previamente no debe haber ningun video asociado al mismo, si se intenta eliminar la API devolvera un error.

#### *Ejemplo 1 (Exito):*
Queremos eliminar el proyecto con `project_id:1`, el cual no tiene videos asociados:

* REQUEST: `DELETE /projects/delete/1`

* RESPONSE: `200 OK`


#### *Ejemplo 2 (Fallo):*
Queremos eliminar un Project con `project_id:2` pero tiene videos asociados:

* REQUEST: `DELETE /projects/delete/2`

* RESPONSE: `409 Conflict`

##### _JSON_ Response:
```json
{
    "detail": "Project could not be deleted, contains 1 or more videos associated."
}
```
